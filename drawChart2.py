'''
Übung 10 - Projekt
Datenbankensysteme
Autoren: Fabio Herrmann, Adrian Winau & Teem Qawaider
'''


import psycopg2
import psycopg2.sql as pgsql
import matplotlib
from matplotlib import pyplot
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import PySimpleGUI as sg
from scipy.stats.stats import pearsonr
import sys

def draw_figure_w_toolbar(canvas, fig1, canvas_toolbar):
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    if canvas_toolbar.children:
        for child in canvas_toolbar.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(fig1, master=canvas)
    figure_canvas_agg.draw()
    toolbar = Toolbar(figure_canvas_agg, canvas_toolbar)
    toolbar.update()
    figure_canvas_agg.get_tk_widget().pack(side='right', fill='both', expand=1)

class Toolbar(NavigationToolbar2Tk):
    def __init__(self, *args, **kwargs):
        super(Toolbar, self).__init__(*args, **kwargs)

def updateCountryList(value):
    global conn, cur, window
    order = "DESC"
    if value == "alph": order = "ASC"
    
    sql = """
        WITH gdp AS 
        (
            SELECT country_code, AVG(gdp_usd)
            FROM public.gdp
            WHERE year >= %(minYear)s AND year <= %(maxYear)s
            GROUP BY country_code
            HAVING AVG(gdp_usd) %(gdpop)s %(gdpvalue)s
        ), co2 AS
        (
            SELECT country_code, AVG(total_t)
            FROM public.co2
            WHERE year >= %(minYear)s AND year <= %(maxYear)s
            GROUP BY country_code
            HAVING AVG(total_t) %(co2op)s %(co2value)s
        ), meat AS
        (
            SELECT country_code, AVG(production_total_t)
            FROM public.meat
            WHERE year >= %(minYear)s AND year <= %(maxYear)s
            GROUP BY country_code
            HAVING AVG(production_total_t) %(meatop)s %(meatvalue)s
        ), population AS
        (
            SELECT country_code, AVG(total)
            FROM public.population
            WHERE year >= %(minYear)s AND year <= %(maxYear)s
            GROUP BY country_code
            HAVING AVG(total) %(popop)s %(popvalue)s
        ), alph (country_code, avg) AS
        (
            SELECT DISTINCT country_code, country_name
            FROM public.country
        )
        SELECT
            c.country_name, c.country_code, g.avg
        FROM
            public.country c
        NATURAL JOIN %(join)s g
        ORDER BY g.avg %(orderBy)s
        ;
    """
    try:
        window["-errorLable-"].update("")
        data = {
            "join": value,
            "orderBy": order,
            "minYear": window["-YEAR_MIN-"].get(), "maxYear": window["-YEAR_MAX-"].get(),
            "gdpop":window["-GDPops-"].get(), "gdpvalue": float(window["-GDPvalue-"].get())*1000000000.0,
            "co2op":window["-CO2ops-"].get(), "co2value": float(window["-CO2value-"].get())*1000.0,
            "meatop":window["-MEATAops-"].get(), "meatvalue": float(window["-MEATAvalue-"].get())*1000.0,
            "popop":window["-POPops-"].get(), "popvalue": float(window["-POPvalue-"].get())*1000000.0,
        }
    except Exception as e:
        window["-errorLable-"].update("Falscher Eingabewert!")
        return

    try: 
        cur.execute(pgsql.SQL(sql % data))
        country_list = cur.fetchall()
        fnames = []
        for f in country_list:
            fnames.append(str(f[0])+" "+str(f[1]))

        window["-COUNTRY LIST-"].update(fnames)
        window["-errorLable-"].update("")
        conn.commit()
    except Exception as e:
        print(e)
        window["-errorLable-"].update("SQL ERROR:2")
        conn.rollback()

def format_coord(x,y):
    global rows
    coords = [(x,y),(x,y)]
    return('Left: {:<10}    Right: {:<}'
            .format(*['({:.3f}, {:.3f})'.format(x, y) for x,y in coords]))

def is_letter(txt):
    for c in txt:
        if (c >= 'a' and c <= 'z') or (c >= 'A' and c <= 'Z'):
            continue
        return False
    return True

def main():

    global window, conn, cur

    # Connect to PostgreSQL Database
    if len(sys.argv) < 6:
        raise Exception("Error: Zu wenig Argumente!")
    conn = psycopg2.connect(dbname=sys.argv[1], user=sys.argv[2], password=sys.argv[3], host=sys.argv[4], port=sys.argv[5])

    cur = conn.cursor()

    # 
    matplotlib.use("TkAgg")
    sg.theme("dark grey 9")

    country_list_column = [
        [sg.Text("Country List"), sg.Button("Refresh", enable_events=True, key="UpdateCL",font=("Helvetica", 9))],
        [sg.HSeparator(),],
        [sg.Listbox(values=[], enable_events=True, size=(15, 34), key="-COUNTRY LIST-", auto_size_text=10)],
        [sg.Text("sort by"), sg.InputCombo(values=["alph","gdp","co2","meat","population"],default_value="alph", size=(8,1), key="-COUNTRY LIST ORDER-", readonly=True)],
    ]

    setting_column = [
        [sg.Text("Settings")],
        [sg.HSeparator(),],
        [sg.Text(''.join([str(x) for x in range(30)]),font=("Helvetica", 9),visible=True,text_color="yellow",key="-errorLable-")],
        [
            sg.Text("Country Code"),
            sg.Input(size=(5, 1), enable_events=True, key="-COUNTRY CODE-"),
        ],
        [sg.Text("")],
        [sg.Input(default_text="1961", size=(10, 1), enable_events=True, key="-YEAR_MIN-"),sg.Text("-"),sg.Input(default_text="2017", size=(10, 1), enable_events=True, key="-YEAR_MAX-"),sg.Text("year")],
        [sg.Text("")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-GDPops-", readonly=True,),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-GDPvalue-"),sg.Text("mrd. $", size=(5,1)),sg.Checkbox("gdp", default=True, key="-GDP-", enable_events=True, text_color="light blue")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-CO2ops-", readonly=True),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-CO2value-"),sg.Text("kt", size=(5,1)),sg.Checkbox("co2", default=True, key="-CO2-", enable_events=True, text_color="green")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-POPops-", readonly=True),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-POPvalue-"),sg.Text("mio.", size=(5,1)),sg.Checkbox("population", default=True, key="-POP-", enable_events=True, text_color="purple")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-GROWTHops-", readonly=True),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-GROWTHvalue-"),sg.Text("%", size=(5,1)),sg.Checkbox("population growth", default=True, key="-GROWTH-", enable_events=True, text_color="pink")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-MEATAops-", readonly=True),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-MEATAvalue-"),sg.Text("kt", size=(5,1)),sg.Checkbox("meat - all", default=True, key="-MEAT ALL-", enable_events=True, text_color="red")],
        [sg.InputCombo(values=[">=","<="], default_value=">=" ,enable_events=True, key="-MEATPops-", readonly=True),sg.Input(size=(10, 1), default_text="0", enable_events=True, key="-MEATPvalue-"),sg.Text("kg", size=(5,1)),sg.Checkbox("meat - person", default=True, key="-MEAT PERSON-", enable_events=True, text_color="orange")],
        [sg.Text("")],
        [sg.HSeparator(),],
        [sg.Text("\nKorrelation")],
        [sg.Text("x:"),sg.InputCombo(values=["gdp","co2","pop","growth","meat","meatp"], enable_events=True, key="-KORRX-", readonly=True),sg.Text("y:"),sg.InputCombo(values=["gdp","co2","pop","growth","meat","meatp"], enable_events=True, key="-KORRY-", readonly=True,)],
        [sg.Text("")],
        [sg.Text(''.join([str(x) for x in range(30)]), key="-korr_res-")],
        [sg.Text("\nIst der Wert grün, ist die Korrelation\naussagekräftig. Ist er rot, dann nicht.\n\n1 ist eine positive lineare Korrelation.\n0 ist keine lineare Korrelation.\n-1 ist eine negative lineare Korrelation.\n\n\n\n\n\n\n\n", font=("Helvetica", 13))],
    ]

    canvas_column = [
        [sg.Text("Output")],
        [sg.HSeparator(),],
        [sg.Canvas(key="-CANVAS1-", size=(400 * 2, 400)),],
        [sg.Canvas(key="-CONTROLS_CV1-"),],
        [sg.Canvas(key="-CANVAS2-", size=(400 * 2, 400)),],
        [sg.Canvas(key="-CONTROLS_CV2-"),],
    ]

    layout = [
        [
            sg.Column(country_list_column),
            sg.VSeperator(),
            sg.Column(setting_column),
            sg.VSeperator(),
            sg.Column(canvas_column),
        ]
    ]

    window = sg.Window(
        "Country Data", 
        layout,
        location= (0,0),
        finalize=True,
        element_justification="center",
        font="Helvetica 16",
    )

    fig1, ax1 = pyplot.subplots()
    fig1 = pyplot.figure(1)
    fig2, ax2 = pyplot.subplots()
    fig2 = pyplot.figure(2)
    #fig1 = pyplot.gcf()
    DPI = fig1.get_dpi()
    # ------------------------------- you have to play with this size to reduce the movement error when the mouse hovers over the figure, it's close to canvas size
    pyplot.figure(1)
    fig1.set_size_inches(400 * 2 / float(DPI), 400 / float(DPI))
    draw_figure_w_toolbar(window["-CANVAS1-"].TKCanvas, fig1, window["-CONTROLS_CV1-"].TKCanvas)
    pyplot.figure(2)
    fig2.set_size_inches(400 * 2 / float(DPI), 400 / float(DPI))
    draw_figure_w_toolbar(window["-CANVAS2-"].TKCanvas, fig2, window["-CONTROLS_CV2-"].TKCanvas)


    window["-errorLable-"].update(value=" ")
    window["-korr_res-"].update(value=" ")

    country_list_order = ""
    #updateCountryList(country_list_order)

    event, values = window.read(timeout=100)
    input = window["-COUNTRY CODE-"].get()
    old_input = ""
    rows = [] #: store the values from sql query
    checkbox = False

    while True:
        event, values = window.read(timeout=100)
        if event == "Exit" or event == sg.WIN_CLOSED:
            break

        if event == "-COUNTRY CODE-":
            text = window["-COUNTRY CODE-"].get()
            if not is_letter(text): window["-errorLable-"].update(value="Nur Buchstaben erlaubt")
            elif len(text) < 3: pass
            else: input = text.upper()[:3]

        if event == "-COUNTRY LIST-":
            el = window["-COUNTRY LIST-"].get()[0]
            window["-COUNTRY CODE-"].update(el[len(el)-3:])
            input = window["-COUNTRY CODE-"].get().upper()
            old_input = ''

        if country_list_order != window["-COUNTRY LIST ORDER-"].get() or event == "UpdateCL":
            country_list_order = window["-COUNTRY LIST ORDER-"].get()
            updateCountryList(country_list_order)

        if input != old_input or event == "-YEAR_MIN-" or event == "-YEAR_MAX-":
            if len(window["-YEAR_MIN-"].get()) < 4 or len(window["-YEAR_MAX-"].get()) < 4: continue

            try:
                window["-errorLable-"].update(value="SQL Anfrage wird ausgeführt...")
                sql="""
                    WITH gdp AS 
                    (
                        SELECT country_code, year, gdp_usd
                        FROM public.gdp
                        WHERE
                            country_code = %(cc)s
                            AND year >= %(minYear)s AND year <= %(maxYear)s
                    ), co2 AS
                    (
                        SELECT country_code, year, total_t
                        FROM public.co2
                        WHERE
                            country_code = %(cc)s
                            AND year >= %(minYear)s AND year <= %(maxYear)s
                    ), meat AS
                    (
                        SELECT country_code, year, supply_per_person_kg, production_total_t
                        FROM public.meat
                        WHERE
                            country_code = %(cc)s
                            AND year >= %(minYear)s AND year <= %(maxYear)s
                    ), population AS
                    (
                        SELECT country_code, year, total, growth
                        FROM public.population
                        WHERE
                            country_code = %(cc)s
                            AND year >= %(minYear)s AND year <= %(maxYear)s
                    )

                    SELECT
                        g.year, g.gdp_usd,
                        p.total, p.growth,
                        cc.total_t,
                        m.production_total_t, m.supply_per_person_kg,
                        c.country_name
                    FROM
                        public.country c
                    NATURAL JOIN gdp g
                    NATURAL JOIN co2 cc
                    NATURAL JOIN meat m
                    NATURAL JOIN population p
                    ;
                """
                data = {
                    'cc':str(input),
                    'minYear': str(window["-YEAR_MIN-"].get()),
                    'maxYear': str(window["-YEAR_MAX-"].get())
                }
                cur.execute(sql, data)
                rows = cur.fetchall()
                conn.commit()
                old_input = ''

                if rows == []: window["-errorLable-"].update(value="Land nicht gefunden")
                else: window["-errorLable-"].update(value=" ")
            except Exception as e:
                print(e)
                window["-errorLable-"].update("SQL ERROR:1")
                conn.rollback()
                continue
        
        # Generate points for sine curve.
        pyplot.figure(1)
        ax1.cla()
        ls = ""

        if event in ["-GDP-","-CO2-","-POP-","-GROWTH-","-MEAT ALL-","-MEAT PERSON-"]:
            checkbox = True
            old_input = ''

        if input != old_input and len(input) == 3:

            if values["-GDP-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[1])
                pyplot.plot(x, y, color='blue')
                ls += "GDP in USD, "
                #ax1.format_coord = format_coord
                #ax1.format_coord = lambda x,y: ""

            if values["-CO2-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[4])
                pyplot.plot(x, y, color='green')
                ls += "CO2 Emission in t, "

            if values["-POP-"] or values["-GROWTH-"]:
                ls += "\n"
                
            if values["-POP-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[2])
                pyplot.plot(x, y, color='purple')
                ls += "Population, "
                
            if values["-GROWTH-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[3])
                pyplot.plot(x, y, color='pink')
                ls += "Population Growth in %, "

            if values["-MEAT ALL-"] or values["-MEAT PERSON-"]:
                ls += "\n"
                
            if values["-MEAT ALL-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[5]*1000)
                pyplot.plot(x, y, color='red')
                ls += "Meat Production in kg, "

            if values["-MEAT PERSON-"]:
                x, y = [], []
                for row in rows:
                    x.append(row[0])
                    y.append(row[6])
                pyplot.plot(x, y, color='orange')
                ls += "Meat Supply per Person in kg"
                
            try:
                ax1.set_title(rows[1][7])
            except:
                ax1.set_title("Country")
            try:
                ax1.set_ylabel(ls)
            except:
                ax1.set_ylabel("Value")
            ax1.set_xlabel("Year")
            ax1.set_yscale('log')
        
            draw_figure_w_toolbar(window["-CANVAS1-"].TKCanvas, fig1, window["-CONTROLS_CV1-"].TKCanvas)    

        if event in ["-KORRX-", "-KORRY-"]:
            old_input = ""

        if input != old_input and len(input) == 3 and not checkbox:
            labels = [None,"GDP in USD","Population total","Population Growth in %","CO2 Emission in t","Meat Production in t","Meat Supply per Person in kg"]
            try:
                index = ["gdp","pop","growth","co2","meat","meatp"].index(window["-KORRX-"].get()) + 1
                indey = ["gdp","pop","growth","co2","meat","meatp"].index(window["-KORRY-"].get()) + 1
            except:
                index, indey = 0, 0

            pyplot.figure(2)
            ax2.cla()

            if index != 0 and indey != 0:
                x, y, n = [], [], []
                for row in rows:
                    if row[index] == None or row[indey] == None:
                        continue
                    x.append(row[index])
                    y.append(row[indey])
                    n.append(row[0])
                ax2.scatter(x, y, color='red')
                for i in range(len(n)):
                    ax2.annotate(n[i], (x[i], y[i]), ha='left', va='center', size=7)

                try:
                    ax2.set_title(rows[1][7])
                except:
                    ax2.set_title("Country")
                ax2.set_ylabel(labels[indey])
                ax2.set_xlabel(labels[index])
                ax2.set_xscale('log')
                ax2.set_yscale('log')

            draw_figure_w_toolbar(window["-CANVAS2-"].TKCanvas, fig2, window["-CONTROLS_CV2-"].TKCanvas)

            try:
                korr_val=pearsonr(x, y)

                if str(korr_val[0]) != "nan":
                    if korr_val[1] < 0.05:
                        window["-korr_res-"].update(value=str(round(korr_val[0],3)), text_color='light green')
                    else:
                        window["-korr_res-"].update(value=str(round(korr_val[0],3)), text_color='red')

            except:
                window["-errorLable-"].update(value="Korrelationsfehler 1")

        else:
            checkbox = False 

        window 
        
        old_input = input

    window.close()

if __name__ == "__main__":
    main()
